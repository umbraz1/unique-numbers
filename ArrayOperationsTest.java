import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayOperationsTest {

    @org.junit.jupiter.api.Test
    void uniques() {
    }

    @org.junit.jupiter.api.Test
    public void TestNull(){
        int [] nullArray = {0};
        ArrayOperations.uniques(nullArray);
 }
    @org.junit.jupiter.api.Test
    public void TestNull2() {
       int []  arr2 = {' '};
     ArrayOperations.uniques(arr2);
 }
    @org.junit.jupiter.api.Test
    public void TestMin(){
       int [] arrayMin = {Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
     ArrayOperations.uniques(arrayMin);
 }
    @org.junit.jupiter.api.Test
    public void TestMax(){
        int [] arrayMax = {Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE};
        ArrayOperations.uniques(arrayMax);
    }
    @org.junit.jupiter.api.Test
    public void TestMinMax(){
        int [] arrayMinMax = {Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE,Integer.MIN_VALUE,Integer.MAX_VALUE};
        ArrayOperations.uniques(arrayMinMax);
    }
    @org.junit.jupiter.api.Test
    public void Test1() {
        int [] arr1 = {1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10};
        ArrayOperations.uniques(arr1);
    }
    @org.junit.jupiter.api.Test
    public void Test2() {
        int [] arr2 = {-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,1,2,3,4,5,6,7,8,9,10};
        ArrayOperations.uniques(arr2);
    }
    @org.junit.jupiter.api.Test
    public void Test3() {
        int [] arr3 = {10,9,8,7,6,5,4,3,2,1,10,9,8,7,6,5,4,3,2,1};
        ArrayOperations.uniques(arr3);
    }
    @org.junit.jupiter.api.Test
    public void Test4() {
        int [] arr4 = {101233121,92131232,844334,72322,6123213,523,44,34,22,1,10,91,-8,73,6,5,94,3,22,11,1,1,1,1,2,3,6,4,7,8,9,8,5,4,3,7,89,4,3,23,4,5,53,53,3,};
        ArrayOperations.uniques(arr4);
    }
}